/**
 * @author: Dakota Kelley
 * @program: Have the arduino move without running into walls.
 * @coffee: Not enough.....
 */
// These are the includes
#include "MeOrion.h"
//#include <MeMCore.h>

// Music Constants
#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978

// Create my melodies

// Mario main theme melody
int melody[] = {
  NOTE_E7, NOTE_E7, 0, NOTE_E7,
  0, NOTE_C7, NOTE_E7, 0,
  NOTE_G7, 0, 0,  0,
  NOTE_G6, 0, 0, 0,
 
  NOTE_C7, 0, 0, NOTE_G6,
  0, 0, NOTE_E6, 0,
  0, NOTE_A6, 0, NOTE_B6,
  0, NOTE_AS6, NOTE_A6, 0,
 
  NOTE_G6, NOTE_E7, NOTE_G7,
  NOTE_A7, 0, NOTE_F7, NOTE_G7,
  0, NOTE_E7, 0, NOTE_C7,
  NOTE_D7, NOTE_B6, 0, 0,
 
  NOTE_C7, 0, 0, NOTE_G6,
  0, 0, NOTE_E6, 0,
  0, NOTE_A6, 0, NOTE_B6,
  0, NOTE_AS6, NOTE_A6, 0,
 
  NOTE_G6, NOTE_E7, NOTE_G7,
  NOTE_A7, 0, NOTE_F7, NOTE_G7,
  0, NOTE_E7, 0, NOTE_C7,
  NOTE_D7, NOTE_B6, 0, 0
};

// Mario main them tempo
int tempo[] = {
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
 
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
 
  9, 9, 9,
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
 
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
 
  9, 9, 9,
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
};

// Underworld melody
int underworld_melody[] = {
  NOTE_C4, NOTE_C5, NOTE_A3, NOTE_A4,
  NOTE_AS3, NOTE_AS4, 0,
  0,
  NOTE_C4, NOTE_C5, NOTE_A3, NOTE_A4,
  NOTE_AS3, NOTE_AS4, 0,
  0,
  NOTE_F3, NOTE_F4, NOTE_D3, NOTE_D4,
  NOTE_DS3, NOTE_DS4, 0,
  0,
  NOTE_F3, NOTE_F4, NOTE_D3, NOTE_D4,
  NOTE_DS3, NOTE_DS4, 0,
  0, NOTE_DS4, NOTE_CS4, NOTE_D4,
  NOTE_CS4, NOTE_DS4,
  NOTE_DS4, NOTE_GS3,
  NOTE_G3, NOTE_CS4,
  NOTE_C4, NOTE_FS4, NOTE_F4, NOTE_E3, NOTE_AS4, NOTE_A4,
  NOTE_GS4, NOTE_DS4, NOTE_B3,
  NOTE_AS3, NOTE_A3, NOTE_GS3,
  0, 0, 0
};
// Underwolrd tempo
int underworld_tempo[] = {
  12, 12, 12, 12,
  12, 12, 6,
  3,
  12, 12, 12, 12,
  12, 12, 6,
  3,
  12, 12, 12, 12,
  12, 12, 6,
  3,
  12, 12, 12, 12,
  12, 12, 6,
  6, 18, 18, 18,
  6, 6,
  6, 6,
  6, 6,
  18, 18, 18, 18, 18, 18,
  10, 10, 10,
  10, 10, 10,
  3, 3, 3
};

// Declare my base variables
MeRGBLed              v_led_obj(PORT_3);    // LED Object
uint8_t               v_led_1 = 1;          // LED Number 1
uint8_t               v_led_2 = 0;          // LED Number 2
uint8_t               v_color_on = 255;     // LED Max Color
uint8_t               v_color_off = 0;      // LED Min Color

MeDCMotor             v_motor_left(M1);     // Motor Left
MeDCMotor             v_motor_right(M2);    // Motor Right

MeUltrasonicSensor    v_sensor(PORT_6);     // This is the Ultra Sonic Sensor

MeBuzzer              v_buzzer;             // This is the buzzer that will sing for us

// This is the main setup code
void setup() {
  
}

// This is the main function
void loop() {

  play();
  
}

// Set LED to red
void led_red() {
  v_led_obj.setColorAt(v_led_1, v_color_on, v_color_off, v_color_off);
  v_led_obj.setColorAt(v_led_2, v_color_on, v_color_off, v_color_off);
  v_led_obj.show();
}

// Set LED to Green
void led_green() {
  v_led_obj.setColorAt(v_led_1, v_color_off, v_color_on, v_color_off);
  v_led_obj.setColorAt(v_led_2, v_color_off, v_color_on, v_color_off);
  v_led_obj.show();
}

// Move the robot forward
void motor_forward(uint8_t p_motor_speed) {
  v_motor_left.run(-p_motor_speed);
  v_motor_right.run(p_motor_speed);
}

// Move the robot backward
void motor_backward(uint8_t p_motor_speed) {
  v_motor_left.run(p_motor_speed);
  v_motor_right.run(-p_motor_speed);
}

// Move the robot left
void motor_left(uint8_t p_motor_speed) {
  v_motor_left.run(p_motor_speed);
  v_motor_right.run(p_motor_speed);
}

// Move the robot right
void motor_right(uint8_t p_motor_speed) {
  v_motor_left.run(-p_motor_speed);
  v_motor_right.run(-p_motor_speed);
}

// Stop the robots movement
void motor_stop(){
  v_motor_left.stop();
  v_motor_right.stop();
}

// Motion
void motion() {
  if (v_sensor.distanceCm() < 20) {
    led_red();
    motor_stop();
    motor_left(100);
  } else {
    led_green();
    motor_forward(100);
  }
}

// Play the music
void play() {
  // Infinate loop music
  while (true) {
    for (uint8_t v_index = 0; v_index < 3; v_index++) {
      
      // if it is last run play the underworld theme
      if (v_index == 2) {
        // get size of underworld melody
        int v_size = sizeof(underworld_melody)/sizeof(int);
        // Loop through melody
        for (int v_this_note = 0; v_this_note < v_size; v_this_note++) {

          // Start motion
          motion();
          
          // Calculate duration of note
          int v_note_duration = 1000 / underworld_tempo[v_this_note];

          // Make Note
          v_buzzer.tone(underworld_melody[v_this_note], v_note_duration);

          // set minimum time between notes
          int v_pause_between_notes = v_note_duration * 1.30;
          delay(v_pause_between_notes);

          // Stop the tone playing
          v_buzzer.noTone();
        }
      } else {
        // get size of mario melody
        int v_size = sizeof(melody)/sizeof(int);
        // Loop through melody
        for (int v_this_note = 0; v_this_note < v_size; v_this_note++) {

          // Start motion
          motion();
           
          // Calculate duration of note
          int v_note_duration = 1000 / tempo[v_this_note];

          // Make Note
          v_buzzer.tone(melody[v_this_note], v_note_duration);

          // set minimum time between notes
          int v_pause_between_notes = v_note_duration * 1.30;
          delay(v_pause_between_notes);

          // Stop the tone playing
          v_buzzer.noTone();
        }
      }
    }
  }
}

